module FindTemplate
  def find_template views, name, engine, &block
    Array(views).each do |v|
      super(v, name, engine, &block) 
    end
  end
end
