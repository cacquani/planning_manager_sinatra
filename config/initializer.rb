require 'sinatra/base'
require 'slim'
require 'sass'
require 'warden'

require './config/datamapper'
require './config/helpers/find_template'
require './main'

class InfinityBuilder < Sinatra::Base
  set :sessions, true

  @builder = Rack::Builder.new do
    Warden::Manager.before_failure do |env, _opts|
      env['REQUEST_METHOD'] = 'POST'
    end

    Warden::Strategies.add(:password) do
      def valid?
        (params['user'] &&
         params['user']['username'] &&
         params['user']['password'])
      end

      def authenticate!
        user = User.first(username: params['user']['username'])
        return fail! unless user

        if user.authenticate(params['user']['password'])
          success!(user)
        else
          fail!('Could not log in. Username or password incorrect.')
        end
      end
    end

    use Rack::MethodOverride
    use Rack::Session::Cookie,
        secret: 'PwOV9XnhDwe8BM-fMBRwTm6q5jfwZa-v9M9BSuRz'
    # use Rack::Flash, accessorize: [:error, :success, :message]

    use Warden::Manager do |config|
      config.serialize_into_session(&:id)
      config.serialize_from_session { |id| User.get(id) }
      config.scope_defaults :default,
                            strategies: [:password],
                            action: 'session/unauthenticated'
      config.failure_app = self
    end

    map '/session' do
      run ::Session
    end

    map '/stylesheets' do
      run ::Stylesheets
    end

    map '/planning' do
      run ::PlanningManager
    end

    map '/finance' do
      run ::FinanceManager
    end

    map '/blog' do
      run ::InfinityBlog
    end

    map '/' do
      run ::InfinityManager
    end
  end

  def self.builder
    @builder
  end
end
