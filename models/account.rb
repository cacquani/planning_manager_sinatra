class Account
  include DataMapper::Resource

  property :id,   Serial
  property :name, String
  belongs_to :balance
  has n, :entries, constraint: :set_nil
end
