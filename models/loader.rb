require './config/datamapper.rb'

require './models/user.rb'
require './models/list.rb'
require './models/task.rb'
require './models/entry.rb'
require './models/account.rb'
require './models/balance.rb'

DataMapper.finalize
DataMapper.auto_upgrade!
