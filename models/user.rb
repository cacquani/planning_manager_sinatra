# encoding: utf-8

# The User model
# At the moment it contains the auth logic only
# Planning to add more ownerships later

class User
  include DataMapper::Resource
  include BCrypt

  attr_accessor :password, :password_confirmation

  property :id,                 Serial,
           key: true
  property :username,           String,
           length: 3..50,  required: true
  property :encrypted_password, BCryptHash,
           required: true
  property :updated_at,         DateTime,
           default: ->(_r, _p) { DateTime.now }
  property :created_at,         DateTime,
           default: ->(r, _p) { DateTime.now if r.new? }

  validates_confirmation_of :password

  def authenticate(attempted_password)
    encrypted_password == attempted_password
  end

  def password=(pass)
    @password = pass
    self.encrypted_password = pass
  end
end
