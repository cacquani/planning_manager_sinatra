class List
  include DataMapper::Resource

  property :id,    Serial
  property :name,  String,  required: true
  has n, :tasks, constraint: :destroy
  belongs_to :user
end
