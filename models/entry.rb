class Entry
  include DataMapper::Resource

  property :id,     Serial
  property :amount, Float
  belongs_to :balance
  belongs_to :account
end
