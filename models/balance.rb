class Balance
  include DataMapper::Resource

  property :id,    Serial
  has n, :entries, costraint: :destroy
  has n, :accounts
  belongs_to :user
end
