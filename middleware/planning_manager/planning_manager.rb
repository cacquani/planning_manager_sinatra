class PlanningManager < Sinatra::Base
  set :views, [settings.root + '/views', settings.root + '/../../views']
  helpers FindTemplate

  enable :method_override # needed for PUT and DELETE verbs
  before /(list|task)/ do
    env['warden'].authenticate!
  end

  #
  # LISTS
  #

  # GET index
  get '/lists' do
    lists = List.all
    slim :'list/index', layout: :'layouts/main', locals: { lists: lists }
  end

  # GET new
  get '/list' do
    slim :'list/new', layout: :'layouts/main'
  end

  # POST create
  post '/list' do
    params['list']['user_id'] = env['warden'].user.id
    list = List.create(params['list'])
    redirect back
  end

  # DELETE delete
  delete '/list/:id' do
    List.get(params['id']).destroy
    redirect back
  end

  #
  # TASKS
  #

  # GET index
  get '/tasks' do
    tasks = Task.all
    slim :'task/index', layout: :'layouts/main', locals: { tasks: tasks }
  end

  # GET show
  get '/task/:id' do
    task = Task.get(params[:id])
    slim :'task/task', layout: :'layouts/main', locals: { task: task }
  end

  # GET new
  get '/task' do
    slim :'task/new', layout: :'layouts/main'
  end

  # POST create
  post '/task' do
    Task.create(params[:task])
    redirect back
  end

  # DELETE delete
  delete '/task/:id' do
    Task.get(params[:id]).destroy
    redirect back
  end

  # PUT edit
  put '/task/:id' do
    task = Task.get(params[:id])
    task.completed_at = task.completed_at.nil? ? Time.now : nil
    task.save
    redirect back
  end
end
