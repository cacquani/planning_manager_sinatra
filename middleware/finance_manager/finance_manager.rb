class FinanceManager < Sinatra::Base
  set :views, [settings.root + '/views', settings.root + '/../../views']
  helpers FindTemplate

  enable :method_override # needed for PUT and DELETE verbs
  before /\// do
    env['warden'].authenticate!
  end

  # GET index
  get '/' do
    balance = Balance.first(:user => env['warden'].user)
    balance ||= Balance.new(:user => env['warden'].user)
    slim :'index', layout: :'layouts/main', locals: { balance: balance }
  end
end
