require './middleware/infinity_blog/admin.rb'

class InfinityBlog < Sinatra::Base
  set :views, settings.root + '/../../views'
  enable :method_override # needed for PUT and DELETE verbs

  #
  # BLOG
  #

  # GET index
  get '/' do
    posts = Post.all
    slim :'blog/index', layout: :'layouts/main', locals: { posts: posts }
  end

  # GET show
  get '/:id' do
    post = Post.get(params[:id])
    slim :'blog/show', layout: :'layouts/main', locals: { post: post }
  end
end
