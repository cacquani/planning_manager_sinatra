class Stylesheets < Sinatra::Base
  # Serving sass stylesheets
  get '/style.css' do
    scss :'style', layout: false
  end
end
