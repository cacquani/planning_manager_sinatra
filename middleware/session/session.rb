class Session < Sinatra::Base
  set :views, [settings.root + '/views', settings.root + '/../../views']
  helpers FindTemplate

  enable :method_override # needed for PUT and DELETE verbs

  get '/new' do
    redirect '/' unless env['warden'].unauthenticated?
    slim :'new', layout: :'layouts/main'
  end

  post '/' do
    env['warden'].authenticate!
    redirect '/'
  end

  delete '/' do
    env['warden'].raw_session.inspect
    env['warden'].logout
    redirect '/'
  end

  post '/unauthenticated' do
    session['return_to'] = env['warden.options'][:attempted_path]
    redirect '/session/new'
  end

  not_found do
    redirect '/new'
  end
end
