# Main module - the dashboard for the application

require './models/loader.rb'
require './middleware/loader.rb'

class InfinityManager < Sinatra::Base
  # InfinityManager is the wrapper for the whole application.
  # We will use it as a home and a dashboard, for now.

  use PlanningManager
  use FinanceManager

  # GET / - the dashboard
  get '/' do
    lists = List.all
    slim :'main/index', layout: :'layouts/main', locals: { lists: lists }
  end
end
